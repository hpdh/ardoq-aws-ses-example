module.exports =  (body, options={}) => {
    const { data, data: { rootWorkspace } } = body;
    const org = body['organization-label'];
    const resourceId = body['resource-id'];
    const eventType = body['event-type'];
    const resourceType = body['resource-type'];
    const visualisation = options.visualisation || 'reader';

    let appLink;
    if (eventType !== 'delete' && resourceType === "component" || resourceType === 'reference') {
        const context = (resourceType === 'reference') ? data.source : resourceId;
        appLink = `https://app.ardoq.com/app/view/${visualisation}/workspace/${rootWorkspace}/component/${context}?org=${org}`
      } 
      else {
        appLink = `https://app.ardoq.com/app/view/reader/workspace/${rootWorkspace}?org=${org}`
      }  
    return appLink;
};