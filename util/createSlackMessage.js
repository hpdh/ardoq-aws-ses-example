module.exports = (body, options={ alertOwner: false, url: null }) => {
    const { data, data: { lastModifiedByEmail, name, owner_slack_member_id } } = body;
    const resourceType = body['resource-type'];
    const eventType = body['event-type'];
    const type = (resourceType === 'reference') ? data.ardoq.typeName : data.type;

    // NB no user or type info for deleted component. why?

    // console.log('creating slack message!', options);
    let slackMessage;

    if (eventType !== 'delete' && resourceType === 'component') {
        slackMessage = `A ${resourceType} called \`${name}\` (type: _${type}_) has been *${eventType + "d"}*`;
    } else if (eventType !== 'delete' && resourceType === 'reference') {
        slackMessage = `A ${resourceType} (type: _${type}_) has been *${eventType + "d"}*`;
    }else if (eventType === 'delete' && resourceType === 'reference') {
        slackMessage = `A ${resourceType} has been *${eventType + "d"}*`;
    } else {
        slackMessage = `A ${resourceType} called \`${name}\` has been *${eventType + "d"}*`;

    }
    
    
    // Include last updated by username only if it exists ( has recently been removed from webhooks etc)
    if (lastModifiedByEmail) {
        slackMessage +=  ` by ${lastModifiedByEmail}`
    }

    // include URL if given
    if (options.url) {
        slackMessage +=  `. See the updates here: ${options.url}`
    }


    console.log('owner_slack_member_id: ', owner_slack_member_id, !!owner_slack_member_id);
    console.log('options.alertOwner: ', options.alertOwner);
    // alert user if option boolean provided
    if (options.alertOwner && owner_slack_member_id) {
        console.log('tagging user in slack!');
        return slackMessage + ` <@${owner_slack_member_id}>`
    }
    return slackMessage
}