/*
    Logic for whether to proceed processing event. Remove unwated event types, subscribe to workspaces or component 
    types etc...
*/

module.exports = (body, options = {}) => {
    const { data, data: { rootWorkspace } } = body;
    const { filter } = options;
    const eventType = body['event-type'];

    // always reject these:
    if (['component-reference-count', 'recalculation-done', 'unset'].indexOf(eventType) >= 0 ) {
        console.log(`${eventType}...no event type match! filtering.`);
        return false;
    } else {
        console.log('eventType', eventType);
    }

    // return true for no filter conditions
    if (!filter) {
        return true;
    }

    // filter workspace conditions

    if (filter.rootWorkspace && filter.rootWorkspace.length) {
        console.log(rootWorkspace)
        if (filter.rootWorkspace.indexOf(rootWorkspace) >= 0) {
            return true;
        }

        console.log('no workspace match! filtering.');
        return false;
    }
    return true;
}