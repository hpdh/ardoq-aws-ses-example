/* 
    Create simple html template for workspace updates
*/
const createEmailBody = (body, options={ url: null, emailFrom: null }) => {
    const { data, data: { lastModifiedByEmail, name, owner_email_address } } = body;
    const resourceType = body['resource-type'];
    const eventType = body['event-type'];
    const type = (resourceType === 'reference') ? data.ardoq.typeName : data.type;

    let emailBody;

    if (eventType !== 'delete' && resourceType === 'component') {
        emailBody = `A ${resourceType} called <b>${name}</b> (type: <i>${type}</i>) has been <b>${eventType + "d"}</b>`;

    } else if (eventType !== 'delete' && resourceType === 'reference') {
        emailBody = `A ${resourceType} (type: <i>${type}</i>) has been <b>${eventType + "d"}</b>`;

    }else if (eventType === 'delete' && resourceType === 'reference') {
        emailBody = `A ${resourceType} has been <b>${eventType + "d"}</b>`;

    } else {
        emailBody = `A ${resourceType} called <b>${name}</b> has been <b>${eventType + "d"}</b>`;
    }

    // include URL if given
    if (options.url) {
        emailBody +=  `. See the updates here: ${options.url}`
    }

    emailBody = '<div>' + emailBody + '</div>'
    return emailBody
}


module.exports = (body, options={ url: null, emailFrom: null }) => {
    return createEmailBody(body, options)
}