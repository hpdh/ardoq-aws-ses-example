const createEmailBody = require('./createEmailBody');

module.exports = (body, options={ url: null, senderEmail: null, additionalAddresses : null }) => {
    const { data: { owner_email_address } } = body;
    const {senderEmail} = options;
    const additionalAddresses = options.additionalAddresses || [];
    const destinationAddresses = (owner_email_address) ? [owner_email_address].concat(additionalAddresses) : additionalAddresses;
    
    if (!senderEmail) throw new Error('Sender email required.')
    if (!destinationAddresses.length) throw new Error('Destination email required.')
    
    const emailParams = {
        Source: senderEmail,
        Destination: { ToAddresses: destinationAddresses },
        // Destination: { ToAddresses: ['harry.howard@ardoq.com'] },
        Message: {
          Body: {
            Html: {
              Charset: 'UTF-8',
              Data: createEmailBody(body, options)
            },
          },
          Subject: {
            Charset: 'UTF-8',
            Data: `You received a message from Ardoq!`
          }
        }
      }

    return emailParams
}