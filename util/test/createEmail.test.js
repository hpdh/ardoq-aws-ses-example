const createEmail = require('../createEmail');

const componentCreate = require('../../mock_data/componentCreate.json');

// console.log(JSON.stringify(createEmail(componentCreate)));

test('createUrl function is defined', () => {
  expect(typeof createEmail).toEqual('function');
});

test('email object adds extra email addresses', () => {
  // expect(createEmail(body, {})).toEqual();
});

test('throws error if no sender email address', () => {
  expect(typeof createEmail).toEqual('function');
});

test('gets owner email address from body', () => {
  expect(typeof createEmail).toEqual('function');
});

test('throws error if no email in either body or options', () => {
  expect(typeof createEmail).toEqual('function');
});


// A component called `Cisco Defense Orchestrator` (type: _Application_) has been *updated* . See the updates here! https://ardoq.io/WuoXgAP
