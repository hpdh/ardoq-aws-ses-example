const filterEvent = require('../filterEvent');
const componentCreate = require('../../mock_data/componentCreate.json');
const componentUpdate = require('../../mock_data/componentUpdate.json');
const componentDelete = require('../../mock_data/componentDelete.json');
const referenceCreate = require('../../mock_data/referenceCreate.json');
const referenceUpdate = require('../../mock_data/referenceUpdate.json');
const referenceDelete = require('../../mock_data/referenceDelete.json');


test('createUrl function is defined', () => {
  expect(typeof filterEvent).toEqual('function');
});

test('returns false for recalculation event types', () => {
    let body = Object.assign(componentCreate, {'event-type': 'recalculation-done'});
    expect(filterEvent(body)).toEqual(false);
})

test('returns false for component-reference-count event types', () => {
    let body = Object.assign(componentCreate, {'event-type': 'component-reference-count'});
    expect(filterEvent(body)).toEqual(false);
})

test('returns true for create event types', () => {
    let body = Object.assign(componentCreate, {'event-type': 'create'});
    expect(filterEvent(body)).toEqual(true);
})

test('returns true for update event types', () => {
    let body = componentUpdate;
    expect(filterEvent(body)).toEqual(true);
})

test('returns true for delete event types', () => {
    let body = componentDelete;
    expect(filterEvent(body)).toEqual(true);
})

test('returns false if event does not match workspace filter conditions ', () => {
    let body = componentCreate;    
    let options = {
        filter: {
            rootWorkspace: [
                'this will not match!!'
            ]
        }
    }
    expect(filterEvent(body, options)).toEqual(false);
})

test('returns true if event matches workspace filter conditions ', () => {
    let body = componentCreate;
    let options = {
        filter: {
            rootWorkspace: [
                body.data.rootWorkspace
            ]
        }
    }
    expect(filterEvent(body, options)).toEqual(true);
})

test('returns false if event does not match component type filter conditions AND type is component ', () => {
    let body = componentCreate;
    body['event-type'] = 'component-reference-count'
    expect(filterEvent(body)).toEqual(false);
})

test('returns true if event matches workspace filter conditions ', () => {
    let body = componentCreate;
    const opttions = 
    body['event-type'] = 'component-reference-count'
    expect(filterEvent(body)).toEqual(false);
})
