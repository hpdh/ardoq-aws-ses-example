const createEmailBody = require('../createEmailBody');

// mock data
const componentCreate = require('../../mock_data/componentCreate.json');


test('createUrl function is defined', () => {
  expect(typeof createEmailBody).toEqual('function');
});

test('email works for component update', () => {
  expect(createEmailBody(componentCreate)).toEqual('<div>A component called <b>My new App</b> (type: <i>Application</i>) has been <b>created</b></div>'
  );
});

test('email works for component update with URL', () => {
  expect(createEmailBody(componentCreate, { url: 'https://ardoq.com/foobar'})).toEqual('<div>A component called <b>My new App</b> (type: <i>Application</i>) has been <b>created</b>. See the updates here: https://ardoq.com/foobar</div>'
  );
});



// A component called `Cisco Defense Orchestrator` (type: _Application_) has been *updated* . See the updates here! https://ardoq.io/WuoXgAP
