const createSlackMessage = require('../createSlackMessage');
const componentCreate = require('../../mock_data/componentCreate.json');
const componentUpdate = require('../../mock_data/componentUpdate.json');
const componentUpdateNoEmail = require('../../mock_data/componentUpdateNoEmail.json');
const componentDelete = require('../../mock_data/componentDelete.json');
const referenceCreate = require('../../mock_data/referenceCreate.json');
const referenceUpdate = require('../../mock_data/referenceUpdate.json');
const referenceDelete = require('../../mock_data/referenceDelete.json');


test('createSlackMessage function is defined', () => {
    expect(typeof createSlackMessage).toEqual('function');
});

test('slack Message works for component create', () => {
  message = createSlackMessage(componentCreate)
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *created* by harry.howard@ardoq.com');
});

test('slack Message works for component update', () => {
  message = createSlackMessage(componentUpdate)
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *updated* by harry.howard@ardoq.com');
});

// NB no user or type info for deleted component
test('slack Message works for component delete', () => {
  message = createSlackMessage(componentDelete)
  expect(message).toEqual('A component called `My new App` has been *deleted*');
});

// references

test('slack Message works for reference create', () => {
  message = createSlackMessage(referenceCreate)
  expect(message).toEqual('A reference (type: _One-way Integration_) has been *created* by harry.howard@ardoq.com');
});

test('slack Message works for reference update', () => {
  message = createSlackMessage(referenceUpdate)
  expect(message).toEqual('A reference (type: _One-way Integration_) has been *updated* by harry.howard@ardoq.com');
});

test('slack Message works for reference delete', () => {
  message = createSlackMessage(referenceDelete)
  expect(message).toEqual('A reference has been *deleted*');
});

// options
test('alert use in slack if option is true', () => {
  message = createSlackMessage(componentUpdate, { alertOwner: true })
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *updated* by harry.howard@ardoq.com <@myslackmemeberID>');
});

test('do not alert user in slack if option is false', () => {
  message = createSlackMessage(componentUpdate, { alertOwner: false })
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *updated* by harry.howard@ardoq.com');
});

test('do not alert user in slack if option is blank', () => {
  message = createSlackMessage(componentUpdate)
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *updated* by harry.howard@ardoq.com');
});

test('append updates message and URL if URL given', () => {
  message = createSlackMessage(componentUpdate, { alertOwner: false, url: 'https://ardoq.com/foobar' })
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *updated* by harry.howard@ardoq.com. See the updates here: https://ardoq.com/foobar');
});

test('exclude lastUpdatedEmail if it does not exist', () => {
  message = createSlackMessage(componentUpdateNoEmail)
  expect(message).toEqual('A component called `My new App` (type: _Application_) has been *updated*');
});


// test('createSlackMessage function is defined', () => {
//   expect(typeof createSlackMessage).toEqual('function');
// });






// const slackMessage = `A ${resourceType} ${entityName} ${ardoqType}has been *${eventType}* ${email}. See the updates here! `;