const createUrl = require('../createUrl');
const componentCreate = require('../../mock_data/componentCreate.json');
const componentDelete = require('../../mock_data/componentDelete.json');
const referenceCreate = require('../../mock_data/referenceCreate.json');
const referenceUpdate = require('../../mock_data/referenceUpdate.json');
const referenceDelete = require('../../mock_data/referenceDelete.json');




test('createUrl function is defined', () => {
  expect(typeof createUrl).toEqual('function');
});

test('component update event yields component URL', () => {
    const url = createUrl(componentCreate);
    expect(url).toEqual("https://app.ardoq.com/app/view/reader/workspace/5d67a737b3da0826e45bab34/component/5494163297687eb79e931af5?org=aviato");
});

test('visualisation defaults to reader', () => {
    const url = createUrl(componentCreate);
    expect(url).toEqual("https://app.ardoq.com/app/view/reader/workspace/5d67a737b3da0826e45bab34/component/5494163297687eb79e931af5?org=aviato");
});

test('visualisation chooses visualisation from options param', () => {
    const options = { visualisation: "blockDiagram"};
    const url = createUrl(componentCreate, options);
    expect(url).toEqual("https://app.ardoq.com/app/view/blockDiagram/workspace/5d67a737b3da0826e45bab34/component/5494163297687eb79e931af5?org=aviato");
});

test('context is workspace with component delete', () => {
    const url = createUrl(componentDelete);
    expect(url).toEqual("https://app.ardoq.com/app/view/reader/workspace/5d67a737b3da0826e45bab34?org=aviato");
});

test('context is of source component with with reference create', () => {
    const url = createUrl(referenceCreate);
    expect(url).toEqual("https://app.ardoq.com/app/view/reader/workspace/5d67a737b3da0826e45bab34/component/5d67a737b3da0826e45bac0e?org=aviato");
});

test('context is of source component with with reference update', () => {
    const url = createUrl(referenceUpdate);
    expect(url).toEqual("https://app.ardoq.com/app/view/reader/workspace/5d67a737b3da0826e45bab34/component/5d67a737b3da0826e45bac0e?org=aviato");
});

test('context is of SOURCE COMPONENT with with reference delete', () => {
    const url = createUrl(referenceDelete);
    expect(url).toEqual("https://app.ardoq.com/app/view/reader/workspace/5d67a737b3da0826e45bab34/component/5d67a737b3da0826e45bac0e?org=aviato");

});