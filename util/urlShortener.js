var request = require("request");
module.exports = longUrl => {
    return new Promise( (resolve, reject) => {
        const options = { method: 'POST',
            url: 'https://ardoq.io/shorten',
            headers: 
                { 'cache-control': 'no-cache',
                'content-type': 'application/json' },
            body: { long_url: longUrl },
            json: true 
        };
        request(options, function (error, response, body) {
            if (error) reject(error);
            resolve(body)
        });

    })

} 

