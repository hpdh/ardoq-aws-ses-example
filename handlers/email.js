const aws = require('aws-sdk');

const config = require('../config');
const { createEmail, createUrl, urlShortener, filterEvent } = require('../util/');
const { senderEmail, additionalAddresses, visualisation, workspaceIds } = config.integrations.email.filter( itm => itm.name === 'aviato')[0];

const ses = new aws.SES({region: 'eu-west-1'});
const additionalAddresses = ['harry.howard@ardoq.com'] // additionally send email notfication to these addresses

exports.handler = function(event, context) {
  const { body } = event;
  if (!filterEvent(body, { filter: {rootWorkspace: workspaceIds}})) return;

  const appLink = createUrl( body, { visualisation } );
  urlShortener(appLink)
  .then( shortUrl => {
    const emailParams = createEmail( body, { url: shortUrl, senderEmail, additionalAddresses } );

      ses.sendEmail(emailParams, function(err, data) {
        if (err) {
            console.log(err, err.stack)
            throw err
        }
        else console.log(data); 
      });

    }).then( function(res, err) {
        console.log('email sent to ...')
        console.log(res);
        if (err) throw(err);

  }).catch(error => {
    console.log(error)
  })
}