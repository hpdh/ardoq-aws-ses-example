var exports = module.exports = {};

exports.global = {
    aws: {
        region: 'eu-west-1'
    }
}

exports.integrations = {
    slack: [
        {
            alertUser: true, // do you want to alrt the user in slack? this will @user ( requires slack memeber ID as component/reference property)
            description: 'Integration from Harry\'s slack channel',
            name: 'aviato',
            path: 'notify/slack/aviato', // endpoint path for lambda fn
            slackChannelEndpoint: 'https://hooks.slack.com/services/<your slack channel webhook URL>',
            visualisation: 'blockDiagram', // what view for the Url
            workspaceIds: [ '<your workspace ID>' ],
        },
    ],
    email: [
        {
            name: 'harryEmail',
            description: 'SES email notifications',
            senderEmail: '<your registered email in SES>', //
            path: 'notify/email/harry', // endpoint path for lambda fn
            visualisation: 'blockDiagram', // what view for the Url
            workspaceIds: [ '<your workspace ID>' ],
            
        }
    ]
};


exports.serverlessConf = () => {
    return {
        region: exports.global.aws.region,
    }
};

exports.serverlessPaths = () => {
    return {
        aviato: exports.integrations.slack.filter( itm => itm.name==='aviato')[0].path,
    }
    
};

